import React, { Component } from 'react';
import './App.css';
import './index.css';
import Menu from './components/Menu.js';
import { BrowserRouter as Router, Route } from "react-router-dom";
import UserCard from './components/UserCard.js';
import TodoCard from './components/TodoCard.js';
import About from './components/About.js';
import Home from './components/Home.js';
import axios from 'axios';

class App extends Component {
  constructor(){
    super()
    this.state = {
      users: [],
      address: [],
      name: "",
      userId: "",
      todos: []
    }
  }
  
  componentDidMount(){
    console.log("component will mount will run ...")
   axios.get('https://jsonplaceholder.typicode.com/users')
      .then(response => {
          console.log(response.data);
          this.setState(() => ({users: response.data}))
          console.log(this.state.users);
      })
      .catch(err => {
          console.log(err);
    })
  }

  getTodo = (userId, userName) => {
     console.log("userid get called", userId)
     axios.get(`https://jsonplaceholder.typicode.com/todos?userId=${userId}`)
      .then(response => {
          console.log(response.data);
          this.setState(() => ({todos: response.data, name: userName, userId: userId}))
          console.log(this.state.name);
      })
      .catch(err => {
          console.log(err);
      })
  }

  render() {
     return(
      <Router className="App">
          <div className="menu-wrapper">
            <Menu />
            <Route path="/home" render={() => <Home {...this.state} getTodo = {this.getTodo} isAuthed={true} exact={true}/>}/>
            <Route path="/about" component={About} exact={true}/>
            <Route path='/users' render={() => <UserCard {...this.state} isAuthed={true} exact={true}/>}/>
            <Route path='/todo/' render={() => <TodoCard {...this.state} isAuthed={true} exact={true}/>}/>
          </div>
      </Router>   
        )
  }
}

export default App;
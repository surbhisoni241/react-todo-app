import React, { Component } from 'react';
import '../index.css';
import { Link } from 'react-router-dom';
import TodoCard from '../components/TodoCard.js';

class UserCard extends Component{
    constructor(props){
        super(props)
        this.state = {}
    }
   
    handleTodoBtn = (event) =>{
        console.log('this', this);
     const userId = event.target.dataset.atr;
     const userName = event.target.dataset.prop;
     //this.props.getTodo(userId, userName)
    }

    render(){
     const users = this.props.users;
     console.log(users);

     return(
            <div className="User-card Container App"> <h3> All Users </h3>
                {users.map(user => {
                    return (
                        <div key={user.id} className="user-card-wrapper">
                          <img src="./assets/img/user.png" className="col-md-2" alt="user-img"/>
                          <ul className="col-md-4">
                             <li className="name"> <h5>Name: {user.name}</h5></li>
                             <li>{user.email}</li>
                          </ul>
                          <ul className="col-md-4">
                             <li className="city">City: {user.address["city"]}</li>
                             <li>Website: {user.company["name"]}</li>
                             <li>catchPhrase: {user.company["catchPhrase"]}</li>
                          </ul>
                          <button className="btn btn-secondary col-md-2"  type="submit" onClick={this.handleTodoBtn} data-atr={user.id} data-prop={user.name} value="Show Todos"><Link to={`/todo/${user.id}`}>Submit<TodoCard userId={user.id} name={user.name}/></Link></button>
                        </div>
                    ) 
                })}
            </div>
        );
    }
}

export default UserCard;
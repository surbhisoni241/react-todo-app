import React from 'react';
import '../index.css';
import { Link } from "react-router-dom";



class Menu extends React.Component{
    constructor(){
        super()
        this.state = {}
    }

    render(){
     return(
            <nav className="navbar navbar-light bg-faded rounded mb-3">
                  <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarCollapse" aria-controls="navbarCollapse" aria-expanded="false" aria-label="Toggle navigation">
                    <span className="navbar-toggler-icon"></span>
                  </button>
                  <div className="collapse navbar-toggleable-md" id="navbarCollapse">
                    <ul className="nav navbar-nav text-md-center justify-content-md-between">
                      <li className="nav-item active"><span className="sr-only">(current)</span>
                         <Link to="/home">Home </Link>
                      </li>
                      <li className="nav-item">
                         <Link to="/about">About</Link>
                      </li>
                      <li className="nav-item">
                        <Link to="/users">Users</Link>
                      </li>
                      <li className="nav-item">
                        <Link to="/todo">Todo</Link>
                      </li>
                      <li className="nav-item dropdown">
                        <a className="nav-link dropdown-toggle" href="http://example.com" id="dropdown01" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Dropdown</a>
                        <div className="dropdown-menu" aria-labelledby="dropdown01">
                         <ul>
                          <li className="dropdown-item" >Action</li>
                          <li className="dropdown-item" >Another action</li>
                          <li className="dropdown-item" >Something else here</li>
                          </ul>
                        </div>
                      </li>
                    </ul>
                  </div>
            </nav>

         
        );
    }
}

export default Menu;

                

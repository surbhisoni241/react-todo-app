import React from 'react';
import '../App.css';
import UserCard from './UserCard.js';
import TodoCard from './TodoCard.js';

class Home extends React.Component{
    constructor(props){
        super(props)
    }
    render(){
        const users = this.props.users;
        const userName = this.props.name;
        const userId = this.props.userId;
        const todos = this.props.todos;

        return(
            <div>
               <div className="App"> 
                <div className="todo-menu">
                  <h2>TODO APP</h2>
                </div>
                <div className="row">
                  <div className="col-md-8">
                    <UserCard users={users} getTodo={this.props.getTodo}/>
                  </div>
                  <div className="col-md-4 float-right todo-card">
                    <TodoCard todos={todos} userName={userName} userId={userId}/>
                  </div>
                </div>
              </div>
            </div>
        );
    }
}

export default Home;